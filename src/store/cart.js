import { createSlice } from "@reduxjs/toolkit";

const cartSlice = createSlice({
    name: "cart",
    initialState: {
        itemList: [],
        qty: 0,
        showCart: false,
    },
    reducers: {
        addToCart(state, action) {
            const newItem = action.payload;

            const existingItem = state.itemList.find(item => item.id === newItem.id);
            if (existingItem) {
                existingItem.qty += 1
                existingItem.totalPrice += newItem.price;
            } else {
                state.itemList.push({
                    id: newItem.id,
                    name: newItem.name,
                    price: newItem.price,
                    qty: 1,
                    totalPrice: newItem.price,
                });
                state.qty++;
            }
        },
        removeCart(state, action) {
            const id = action.payload;
            const findItem = state.itemList.find((item) => item.id === id);
            if(findItem.qty === 1) {
                state.itemList = state.itemList.filter((item) => item.id !== id);
                state.qty--;
            } else {
                findItem.qty--;
                findItem.totalPrice -= findItem.price;
            }
        },
        setShowCart(state) {
            state.showCart = true;
        }
    },
})

export const cartActions = cartSlice.actions;

export default cartSlice;