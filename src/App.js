import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./App.css";
import Auth from "./components/Auth";
import Layout from "./components/Layout";
import Notification from "./components/Notification";
import { uiActions } from "./store/ui-slice";
let isFirstRender = true;
function App() {
  const notification = useSelector((state) => state.ui.notification);
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();
  useEffect(() => {
    if(isFirstRender) {
      isFirstRender = false;
      return;
    }
    const sendCartData = async () => {
      dispatch(uiActions.showNotification({
        message: "Sending Request",
        type: "warning",
        opn: true,
      }));
      const res = await fetch("https://redux-a618b-default-rtdb.firebaseio.com/cartItems.json", {
        method: "PUT",
        body: JSON.stringify(cart),
      })
      const data = await res.json();
      console.log(data);
      dispatch(uiActions.showNotification({
        message: "Sent Request to Database Successfully",
        type: "success",
        opn: true,
      }));
    }

    sendCartData().catch(err => {
      dispatch(uiActions.showNotification({
        message: "Sent Request Failed",
        type: "warning",
        opn: true,
      }));
    });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cart]);
  // console.log(cart);
  return (

    <div className="App">
      {notification && (<Notification type={notification.type} message={notification.message}/>)}
      { !isLoggedIn && <Auth />}
      { isLoggedIn && <Layout />}
    </div>
  );
}

export default App;
