import React from "react";
import CartItem from "./CartItem";
import "./Cart.css";
import { useSelector } from "react-redux";
const CartItems = () => {
  const items = useSelector((state) => state.cart.itemList);
  return (
    <div className="cart-container">
      <h2>Your Cart</h2>
      <ul>
        {items.map((item)=>(
          <li key={item.id}>{" "}<CartItem id={item.id} price={item.price} name={item.name} totalPrice={item.totalPrice} qty={item.qty} /></li>
        ))}
      </ul>
    </div>
  );
};

export default CartItems;
