import React from "react";
import { useDispatch } from "react-redux";
import "./Cart.css";
import { cartActions } from "./../store/cart";
const CartItem = ({ name, qty, totalPrice, price, id }) => {
  const dispatch = useDispatch();
  const addHandler = () => {
    dispatch(
      cartActions.addToCart({
        id,
        name,
        price,
      })
    );
  };

  const decrementItem = () => {
    dispatch(cartActions.removeCart(id));
  }
  return (
    <div className="cartItem">
      <h2> {name}</h2>
      <p>${price} /-</p>
      <p>x{qty}</p>
      <article>Total ${totalPrice}</article>
      <button className="cart-actions" onClick={decrementItem}>
        -
      </button>
      <button className="cart-actions" onClick={addHandler}>
        +
      </button>
    </div>
  );
};

export default CartItem;
